<?php
class User
{
    function __construct($username, $hash, $role)
    {
        $this->username = $username;
        $this->hash = $hash;
        $this->role = $role;
    }
}

$user1 = new User("Poseidon", "hash1", "Tank");
$user2 = new User("Hades", "hash2", "Stealth");
$user3 = new User("Ares", "hash3", "Offense");
$user4 = new User("Zeus", "hash4", "Magic");

$godusers = array($user1, $user2, $user3, $user4);

class Insert
{
    function arrayInsert($array)
    {
        foreach ($array as $value)
        {
            $conn = new mysqli("localhost", "root", "", "myapp");
			$query = $conn->prepare("INSERT INTO user (username, hash, role) VALUES (?, ?, ?)");
			$query->bind_param('sss', $value->username, $value->hash, $value->role);
			$query->execute();
        }
    }
}

$insert1 = new Insert;
$insert1->arrayInsert($godusers);
?>