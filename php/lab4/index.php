<?php
function render($page) {
    ob_start();
    include $page . '.php';
    $content = ob_get_clean();
    include 'layout.php';
}

if ($_SERVER['REQUEST_METHOD'] == "GET")
{
	if ((isset($_GET['action']) && $_GET['action'] == "insert"))
	{
		render('form');
	}
	else
	{
		render('list');
	}
}
elseif ($_SERVER['REQUEST_METHOD'] == "POST")
{
	render('thanks');
}
?>