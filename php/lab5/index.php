<?php
if ($_SERVER['REQUEST_METHOD'] == "GET")
{
	if ((isset($_GET['action']) && $_GET['action'] == "insert"))
	{
		include 'view.php';
		$v = new View('form.php', NULL);
		$v->render();
	}
	else
	{
		include 'view.php';
		include 'recipemodel.php';
		$RM = new RecipeModel;
		$v = new View('list.php', $RM->findAll());
		$v->render();
	}
}
elseif ($_SERVER['REQUEST_METHOD'] == "POST")
{
	include 'view.php';
	$v = new View('thanks.php', NULL);
	$v->render();
}
?>