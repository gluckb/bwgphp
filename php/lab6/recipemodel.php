<?php
include 'model.php';
class Recipe
{
		public $id;
		public $title;
		public $ingredient0;
		public $ingredient1;
		public $ingredient2;
		public $instructions;
		
        function __construct($id, $title, $ingredient0, $ingredient1, $ingredient2, $instructions)
        {
                $this->id = $id;
                $this->title = $title;
                $this->ingredient0 = $ingredient0;
                $this->ingredient1 = $ingredient1;
                $this->ingredient2 = $ingredient2;
                $this->instructions = $instructions;
        }
}
class RecipeModel extends Model
{
        function findAll()
        {
				include ('connection.php');
				$conn = get_connection();
				$query = "Select * from Recipe";
				$res = $conn->query($query);
				
				while ($row = $res->fetch_assoc())
				{
					$dummyData[] = new Recipe($row['ID'], $row['title'], $row['ingredient0'], $row['ingredient1'], $row['ingredient2'], $row['instructions']);
				}
				return $dummyData;
                $res->free();
				
				$conn->close();
        }
		
		function insertData()
		{
			include ('connection.php');
			$conn = get_connection();
			$query = $conn->prepare("INSERT INTO Recipe (title, ingredient0, ingredient1, ingredient2, instructions) VALUES (?, ?, ?, ?, ?)");
			$query->bind_param('sssss', $_POST["title"], $_POST["ingredient0"],	$_POST["ingredient1"],	$_POST["ingredient2"], $_POST["instructions"]);
			$query->execute();
		}
}
?>