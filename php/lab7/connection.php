<?php
function get_connection(){
// Running on OpenShift (production)
    if (isset($_ENV['OPENSHIFT_APP_NAME'])) {
        define("DB_NAME", "BWGPHP");
        define("DB_HOST", $_ENV['OPENSHIFT_MYSQL_DB_HOST']);
        define("DB_USER", $_ENV['OPENSHIFT_MYSQL_DB_USERNAME']);
        define("DB_PASS", $_ENV['OPENSHIFT_MYSQL_DB_PASSWORD']);
    }
    else {
        define("DB_NAME", "test");
        define("DB_HOST", "localhost");
        define("DB_USER", "root");
        define("DB_PASS", "");
    }


    $conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if (mysqli_connect_errno()) {
        echo "No connection here... Move along now...";
        die;
    }

    return $conn;
}
?> 