<?php
if ($_SERVER['REQUEST_METHOD'] == "GET")
{
        if ((isset($_GET['action']) && $_GET['action'] == "insert"))
        {
                include 'view.php';
				include 'recipemodel.php';
				$RM = new RecipeModel;
                $v = new View('form.php', $RM->findAll());
                $v->render();
        }
		elseif ((isset($_GET['action']) && $_GET['action'] == "delete"))
		{
				include 'view.php';
                include 'recipemodel.php';
                $RM = new RecipeModel;
                $v = new View('delete.php', $RM->findAll());
                $v->render();
		}
        else
        {
                include 'view.php';
                include 'recipemodel.php';
                $RM = new RecipeModel;
                $v = new View('list.php', $RM->findAll());
                $v->render();
        }
}
elseif ($_SERVER['REQUEST_METHOD'] == "POST")
{
		if ((isset($_GET['action']) && $_GET['action'] == "insert"))
		{
			include 'view.php';
			include 'recipemodel.php';
			$RM = new RecipeModel;
			$v = new View('thanks.php', $RM->insertData());
			$v->render();
		}
		elseif ((isset($_GET['action']) && $_GET['action'] == "delete"))
		{
			include 'view.php';
			include 'recipemodel.php';
			$RM = new RecipeModel;
			$v = new View('list.php', $RM->delete());
			$v->render();
		}
}
?>