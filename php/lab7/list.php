<a href="index.php?action=insert">Insert a recipe</a>

<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Title</th>                  
            <th>Ingredient0</th>
            <th>Ingredient1</th>
            <th>Ingredient2</th>
            <th>Instructions</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($this->data as $recipe) { ?>     
        <tr>
            <td><?php echo htmlentities($recipe->id); ?></td>
            <td><?php echo htmlentities($recipe->title); ?></td>                                
            <td><?php echo htmlentities($recipe->ingredient0); ?></td>
            <td><?php echo htmlentities($recipe->ingredient1); ?></td>
            <td><?php echo htmlentities($recipe->ingredient2); ?></td>
            <td><?php echo htmlentities($recipe->instructions); ?></td>
			<td>
			</td>
        </tr>                                
    <?php }?>          
    </tbody>                
</table>
<form action="" method="post">
	<label>Select Recipe to Delete</label>
    <input class="input-xxlarge" type="text" placeholder="Recipe#" name="delete">
	<a class="btn btn-primary" href="index.php?action=delete"><i class="icon-trash icon-white"></i> Delete</a>
</form>
<br/>
<br/>
