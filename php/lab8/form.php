<form action="" method="post">
        <fieldset>
        <legend>Insert a recipe</legend>
                                        
        <label>Title</label>
        <input class="input-xxlarge" type="text" placeholder="Recipe" name="title">
                                                                
                                
        <label>Ingredients</label>
        <div><input class="input-xlarge" type="text" placeholder="Ingredient0" name="ingredient0"></div>
        <div><input class="input-xlarge" type="text" placeholder="Ingredient1" name="ingredient1"></div>
        <div><input class="input-xlarge" type="text" placeholder="Ingredient2" name="ingredient2"></div>
		<div><input class="input-xlarge" type="text" placeholder="Ingredient3" name="ingredient3"></div>
		<div><input class="input-xlarge" type="text" placeholder="Ingredient4" name="ingredient4"></div>
		<div><input class="input-xlarge" type="text" placeholder="Ingredient5" name="ingredient5"></div>
                                                                
        <label>Instructions</label>
        <div><textarea name="instructions" class="input-block-level" rows="5"></textarea></div>                         
                                
        <button type="submit" class="btn btn-primary">Submit</button>
        
        </fieldset>
</form>
<a href="index.php">Return to recipe list</a>

<?php
//Bryan: Don't think I need to change much here either.
?>