<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab8</title>
        <link href="../css/default.css" rel="stylesheet" media="screen">
        <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<script src="../js/thejs.js" language="Javascript" type="text/javascript"></script>
    </head>
    <body>                
        <div class="container">
                <h1 class="title">Awesome recipe site</h1>
                         
                    <?php echo $content; 
					//Bryan: Think the layout is just fine.
					?>
                        
                </div>       
        
        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>