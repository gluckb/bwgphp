<h2>Thank you for submitting <?php echo htmlentities($_POST['title']); ?></h2>

<h3>Ingredients</h3>
<ul>
        <li><?php echo htmlentities($_POST['ingredient0']); ?></li>
        <li><?php echo htmlentities($_POST['ingredient1']); ?></li>
        <li><?php echo htmlentities($_POST['ingredient2']); ?></li>
		<li><?php echo htmlentities($_POST['ingredient3']); ?></li>
		<li><?php echo htmlentities($_POST['ingredient4']); ?></li>
		<li><?php echo htmlentities($_POST['ingredient5']); ?></li>
		<?php
		//Bryan: I think I made all necessary changes here. We can hard code the form. I am doing up to 6 ingredients for this assignment.
		?>
</ul>
<h3>Instructions</h3>
<p><?php echo htmlentities($_POST['instructions']); ?></p>
<a href="index.php">Return to recipe list</a>